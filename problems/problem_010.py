# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    if number % 3 == 0: # if statement needed, hint given in line 2
                        # hint % has to be used, meaning if 0 is the remainder
                        # then "fizz" is returned. 
        return "fizz"
    else:
        return number
    

